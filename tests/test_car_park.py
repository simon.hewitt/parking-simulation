"""
simon hewitt
July 2022
simon.d.hewitt@gmail.com
07799 381001
"""

import numpy as np
import pytest

from park_implementation import CarPark, ParkingPolicy, Car, Park

CAR_PARK_SIZE = 20


class TestCarPark:
    @pytest.fixture
    def car_park(self):
        return CarPark(CAR_PARK_SIZE, policy=ParkingPolicy.BestFit)

    def test_empty(self, car_park):
        free_slots_indices, free_slot_lengths = car_park.get_slots()
        # Empty car park, one free slot, the size of the car prk
        assert car_park.free_slot_lengths == [CAR_PARK_SIZE]
        assert car_park.free_slots_indices == [0]

    def test_simple_parking(self, car_park):
        slot = car_park.get_slot(1)
        assert slot == 0
        slot = car_park.get_slot(1)
        assert slot == 0

    # @pytest.mark.skip("later")
    def test_no_parking(self, car_park):
        # Fill the car park!
        car_park.car_park_index[:] = 1
        slot = car_park.get_slot(1)
        assert slot is None
        slot = car_park.get_slot(1)
        assert slot is None

    def test_some_parking(self, car_park):
        # Fill the car park! and then make some spaces
        car_park.car_park_index[:] = 1
        car_park.car_park_index[3:5] = 0  # 2-slot space
        car_park.car_park_index[7:10] = 0  # 3-slot space
        car_park.car_park_index[12:14] = 0  # 2-slot space
        slot = car_park.get_slot(1)
        assert slot == 3
        slot = car_park.get_slot(2)
        assert slot == 3  # One slot the exact size
        slot = car_park.get_slot(1)
        assert slot == 3  # no exact, smallest
        slot = car_park.get_slot(3)
        assert slot == 7  # One slot the exact size

    def test_some_more_parking(self):
        car_park = CarPark(car_park_size=5, policy=ParkingPolicy.FirstFit)
        car_park.car_park_index = np.array([0, 1, 0, 1, 0])

        free_slots, free_lengths = car_park.get_slots()
        assert list(free_slots) == [0, 2, 4]
        assert list(free_lengths) == [1, 1, 1]

    def test_reserve_parking(self):
        car_park_size = 12
        car_park = CarPark(car_park_size=car_park_size, policy=ParkingPolicy.FirstFit)
        # park 3 X size 4
        for _ in range(3):
            slot = car_park.park(4)
            assert slot is not None
        slot = car_park.park(4)
        assert slot is None

        car_park = CarPark(car_park_size=car_park_size, policy=ParkingPolicy.FirstFit)
        # park 4 X size 3
        for _ in range(4):
            slot = car_park.park(3)
            assert slot is not None
        slot = car_park.park(4)
        assert slot is None

        car_park = CarPark(car_park_size=car_park_size, policy=ParkingPolicy.FirstFit)
        # park 2 X size 5
        for _ in range(2):
            slot = car_park.park(5)
            assert slot is not None
        spaces = car_park.get_utilisation()
        assert spaces == (2, 12, 200.0 / 12.0)
        slot = car_park.park(5)
        assert slot is None

    def test_utilisation(self):
        car_park_size = 12
        car_park = CarPark(car_park_size=car_park_size, policy=ParkingPolicy.FirstFit)
        car_park.car_park_index = np.array([1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1])
        ut = car_park.get_utilisation()
        assert ut == (6, 12, 50.0)
        slot = car_park.park(2)
        ut = car_park.get_utilisation()
        assert ut == (4, 12, 100 * 4 / 12)
        assert slot == 2
        slot = car_park.park(3)
        ut = car_park.get_utilisation()
        assert ut == (1, 12, 100.0 / 12.0)
        assert slot == 7


class TestCar:
    """Not much to test here"""

    def test_car(self):
        c1 = Car(1, 10)
        assert c1.car_length == 1
        assert c1.car_time == 10
        assert str(c1) == "Car length: 1, parking duration: 10"

    def test_bad_car(self):
        # Ensure params are required
        with pytest.raises(TypeError) as exc:
            c1 = Car(1)

        with pytest.raises(TypeError) as exc:
            c1 = Car(car_time=11)


class TestParking:
    """
    Test the Park class, the main operational class
    """

    def test_cars_expire(self):
        """Test simple parking and expiry as elapse_period() called"""
        car_park = Park(
            park_length=CAR_PARK_SIZE, parking_policy=ParkingPolicy.FirstFit
        )
        c1 = Car(car_length=2, car_time=3)
        c2 = Car(car_length=3, car_time=4)
        assert car_park.report_utilisation() == 0.0
        park1 = car_park.park_car(c1)
        assert park1
        assert car_park.report_utilisation() == 2.0 / CAR_PARK_SIZE
        park2 = car_park.park_car(c2)
        assert park2
        assert car_park.report_utilisation() == 5.0 / CAR_PARK_SIZE
        assert c1.exit_time == 3
        assert c2.exit_time == 4
        assert c1.slot == 0
        assert c2.slot == 2
        car_park.elapse_period()
        car_park.elapse_period()
        assert c1.slot == 0
        assert c2.slot == 2
        car_park.elapse_period()
        assert c1.slot == 0
        assert c2.slot == 2

        car_park.elapse_period()
        # 4 periods, car c1 now expired, check slot is free etc
        assert c1.slot is None
        assert car_park.report_utilisation() == 3.0 / CAR_PARK_SIZE
        assert c2.slot == 2

        car_park.elapse_period()
        # 5 periods, car c2 also expired
        assert car_park.report_utilisation() == 0.0
        assert c2.slot is None
        car_park.elapse_period()

    def test_exact_parking(self):
        car_park = Park(park_length=CAR_PARK_SIZE, parking_policy=ParkingPolicy.BestFit)
        assert car_park.report_utilisation() == 0.0

        cars_parked = []
        for i in range(5):
            c = Car(car_length=i + 1, car_time=i + 1)
            park = car_park.park_car(c)
            cars_parked.append(c)
            assert park
        assert car_park.report_utilisation() == 15 / CAR_PARK_SIZE

        c = Car(car_length=6, car_time=2)
        park = car_park.park_car(c)
        # should be 15/20 full, cannot park L6 car
        assert not park
        assert car_park.report_utilisation() == 15 / CAR_PARK_SIZE

        # But can park 2 X car with len 3, 2
        c = Car(car_length=3, car_time=3)
        park = car_park.park_car(c)
        assert park
        cars_parked.append(c)
        c = Car(car_length=2, car_time=3)
        park = car_park.park_car(c)
        assert park
        cars_parked.append(c)
        assert car_park.report_utilisation() == 1.0

        # parked Length Pattern is now 1,2,3,4,5,3,2
        # Free up the 4 and 2nd 3,
        # test park(3) use the exact (3) slot
        car_park.unpark_car(cars_parked[3])
        car_park.unpark_car(cars_parked[5])
        assert car_park.report_utilisation() == 13 / CAR_PARK_SIZE
        c = Car(car_length=3, car_time=3)
        park = car_park.park_car(c)
        assert park
        assert c.slot == 15  # And not the free slot length 4 at index 6
        c = Car(car_length=3, car_time=3)
        park = car_park.park_car(c)
        assert park
        assert c.slot == 6  # Now takes slot len 4 as it's all thats left
