"""
simon hewitt
July 2022
simon.d.hewitt@gmail.com
07799 381001
"""
from collections import defaultdict
from typing import Union
from dataclasses import dataclass
from enum import Enum

import numpy
import numpy as np
from numpy import ndarray


class ParkingPolicy(Enum):
    FirstFit = 1
    BestFit = 2


@dataclass
class Car:
    """
    main `Car` class, describes a car, with public attributes (below)
    and `internal` attributes used when it is parked, the exit_time and slot occupied
    """

    car_length: int
    car_time: int

    def __post_init__(self):
        # Instance variables but I don't want them to be settable by the caller
        self.exit_time = None  # Set when parked, now + car_time
        self.slot = None  # the parking slot

    def __repr__(self):
        return f"Car length: {self.car_length}, parking duration: {self.car_time}"


class CarPark:
    """
    Class to manage the Car Park internals.
    Uses a NumPy array for speed, just 0==free, 1 == occupied
    """

    def __init__(self, car_park_size: int, policy: ParkingPolicy):
        self.car_park_size = car_park_size
        # zero is FREE, one is BUSY
        self.car_park_index = np.zeros(car_park_size, dtype=int)
        self.free_slots_indices = None
        self.free_slot_lengths = None
        self.exact = policy == ParkingPolicy.BestFit

    def get_slots(self) -> (ndarray, ndarray):
        """
        Internal method, sets two arrays on the class:
        self.free_slots_indices zero or more indices of available slots
        self.free_slot_lengths matching array of the lengths of these slots
        :return: None
        """
        df = np.diff(self.car_park_index)  # diff between array[n+1] to array [n]
        free_slots_indices = np.where(df == -1)[0] + 1
        # diff[0] is the diff between [0] and [1], if self.car_park_index[0] is free ([0]), add an extra free slot
        if self.car_park_index[0] == 0:
            # If it starts with a free slot, have to add this in as diff starts with index +1
            free_slots_indices = np.insert(free_slots_indices, 0, 0)
        filled_slots = np.where(df == 1)[0]
        if len(filled_slots) < len(free_slots_indices):
            # correct for free slot at the end
            filled_slots = np.append(filled_slots, self.car_park_size - 1)

        free_slot_lengths = (filled_slots + 1) - free_slots_indices
        if len(free_slot_lengths) == 0 and len(free_slots_indices) > 0:
            free_slot_lengths = np.array([self.car_park_size])

        # we now have 2 arrays, free_slot_lengths of slot lengths,
        # and free_slots_indices the corresponding index into self.car_park_index
        self.free_slots_indices = free_slots_indices
        self.free_slot_lengths = free_slot_lengths
        return free_slots_indices, free_slot_lengths

    def get_slot(self, size: int) -> Union[int, None]:
        """
        Gets a slot to park a car of specified size
        :param size: Length of the slot, must be > 0 < car park size
        :return: Index on the available slot or None
        """
        self.get_slots()  # reload the available slots

        if not self.exact:
            slot_index = numpy.where(self.free_slot_lengths >= size)[0]
            if len(slot_index) == 0:
                return None  # No slots big enough
            return self.free_slots_indices[slot_index][0]

        # exact, look for exact slot first:
        slot_index = numpy.where(self.free_slot_lengths == size)[0]
        if len(slot_index) > 0:
            return self.free_slots_indices[slot_index][0]

        # No exact slot, find nearest size
        slot_index = numpy.where(self.free_slot_lengths >= size)[0]
        if len(slot_index) == 0:
            return None  # No slots big enough

        out = slot_index[self.free_slot_lengths[slot_index].argmin()]
        return self.free_slots_indices[out]

    def park(self, size: int) -> Union[int, None]:
        slot = self.get_slot(size)
        if slot is None:
            return None  # No spaces!
        self.car_park_index[slot : slot + size] = 1  # Book the slot(s)
        return slot

    def unpark(self, car: Car):
        """
        instance Car must be 'parked' and so contain slot and car_length
        :param car: Car instance to be removed from park
        :return: True if OK, False on any error
        """
        if not car.slot or car.car_length <= 0:
            # Log error here - likely to be a code error
            return False

        self.car_park_index[car.slot : car.slot + car.car_length] = 0
        return True

    def get_utilisation(self) -> (int, int, float):
        """Return tuple free, total slots, %free"""
        spaces_array = np.where(self.car_park_index == 0)
        free = len(spaces_array[0])
        return free, self.car_park_size, 100.0 * free / self.car_park_size


class Park:
    """
    The operational class that manages parking of Car instances
    """

    def __init__(
        self, park_length: int, parking_policy: ParkingPolicy = ParkingPolicy.BestFit
    ):
        assert park_length > 0  # internal code error if <= 0
        self.car_park = CarPark(park_length, policy=parking_policy)
        self.car_park_length = park_length
        self.parking_policy = parking_policy
        self.parked_cars = defaultdict(
            list
        )  # All cars created are stored in thi dict keyed on exit_time
        self.now = 0  # time ticker

    def park_car(self, car: Car) -> bool:
        """
        car is updated with parking slot and exit time
        :param car: the Car instance to be parked
        :return: True if parked, False if not
        """
        slot = self.car_park.park(car.car_length)
        if slot is None:
            return False  # No space!
        car.slot = slot
        xtime = car.exit_time = self.now + car.car_time
        self.parked_cars[xtime].append(car)
        return True

    def elapse_period(self) -> int:
        """
        Increments the time ticker,
        frees any space for cars now expired,
        deleted the parked_cars dict element for this time_slot
        returns the new time ticker"""
        for dead_car in self.parked_cars[self.now]:
            self.unpark_car(dead_car, dont_remove_from_list=True)
        del self.parked_cars[self.now]
        self.now += 1

        return self.now

    def unpark_car(self, dead_car: Car, dont_remove_from_list: bool = False):
        """
        remove a car from the parking slots and from the parked_cars dict
        :param dead_car: The car to be removed
        :param dont_remove_from_list: if True,  remove from parked_cars
        :return:
        """
        if not dont_remove_from_list:
            self.parked_cars[dead_car.exit_time].remove(dead_car)
        self.car_park.unpark(dead_car)
        dead_car.slot = None

    def report_utilisation(self) -> float:
        """Return sum (all cars length) / car park length, 0..1"""
        x = sum(sum(c.car_length for c in cars) for cars in self.parked_cars.values())
        # x = sum(c.car_length for c in self.parked_cars)
        return float(x / self.car_park_length)
