test:
	pytest --cov --cov-report=html
	open htmlcov/index.html

simulate:
	time python simulation.py
	open car_park.csv

docker:
	docker build -t parking .
	docker run parking
