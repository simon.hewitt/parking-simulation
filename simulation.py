"""
simon hewitt
July 2022
simon.d.hewitt@gmail.com
07799 381001
 Simulation file, exercises parking over a number of time slots and
 random cars

The requirement is to simulate 1000 cars over 300 periods, 3.33 cars per period!
The approach adopted is to simulate exactly 300 periods, and randomly choose one of (3,4) cars per period


"""

import csv
import random

# Test parameters
from dataclasses import dataclass
from typing import List

from tabulate import tabulate
from tqdm import trange

from park_implementation import Car, Park

CAR_PARK_SIZE = 10_000
MAX_CAR_SIZE = 50
SIMULATION_PERIODS = 300
MAX_CARS = 1000
MAX_PARKING_DURATION = 100


def get_cars() -> List[Car]:
    """
    Iterator to return a random-length list of cars with random length and duration.
    returns SIMULATION_PERIODS lists
    returns: list[cars]
    """
    average_cars = MAX_CARS / SIMULATION_PERIODS
    random_car_count = (int(average_cars), int(average_cars + 1))
    for _ in trange(SIMULATION_PERIODS):
        cars = []
        for _ in range(random.choice(random_car_count)):
            random_length = random.randrange(1, MAX_CAR_SIZE + 1)
            random_duration = random.randrange(1, MAX_PARKING_DURATION + 1)
            cars.append(Car(car_length=random_length, car_time=random_duration))
        yield (cars)


@dataclass
class Simulate:
    car_park_size: int

    def __post_init__(self):
        self.car_park = Park(CAR_PARK_SIZE)

    def __call__(self, *args, **kwargs):
        """
        Run the simulation
        :param args: none
        :param kwargs: none
        :return: None
        """
        with open("car_park.csv", "w") as csvfile:
            fieldnames = ["Period", "Utilisation", "Failed Parking"]
            csvwriter = csv.DictWriter(csvfile, fieldnames=fieldnames)
            csvwriter.writeheader()

            for period, car_list in enumerate(get_cars()):
                failed_parkings = sum(
                    not self.car_park.park_car(car) for car in car_list
                )
                utilisation = self.car_park.report_utilisation()
                csvwriter.writerow(
                    {
                        "Period": period,
                        "Utilisation": utilisation * 100.0,
                        "Failed Parking": failed_parkings,
                    }
                )
                self.car_park.elapse_period()

            # That's all the cars, run periods until the car park is empty
            while (utilisation := self.car_park.report_utilisation()) > 0.0:
                period += 1
                csvwriter.writerow(
                    {
                        "Period": period,
                        "Utilisation": utilisation * 100.0,
                        "Failed Parking": failed_parkings,
                    }
                )
                self.car_park.elapse_period()


def print_simulation_conditions():
    print("Car parking simulation, test conditiions:")
    table = [
        ["Car park size", CAR_PARK_SIZE],
        ["Periods", SIMULATION_PERIODS],
        ["Total cars", MAX_CARS],
        ["Max car size", MAX_CAR_SIZE],
        ["Max parking periods", MAX_PARKING_DURATION],
        ["Author", "Simon Hewitt, simon.d.hewitt@gmail.com, 07799 381001"],
    ]
    print(tabulate(table))


""" MAIN """
if __name__ == "__main__":
    print_simulation_conditions()
    simulation = Simulate(CAR_PARK_SIZE)
    simulation()
