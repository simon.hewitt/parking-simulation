# Car Park Simulation





> Simon Hewitt

> simon.d.hewitt@gmail.com

> 07799 381001



This is a simple simulation of a simplified car parking model.



##  Summary

I believe I have met all the requirements of the task.

Overall the simulation runs at close to **O(n)** (see breakdown below).

The simulation of 10,000 slots and 1000 cars in 300 periods takes 0.26 seconds:

`python simulation.py  0.26s user 0.30s system 325% cpu 0.171 total` (38,400 iterations/second)

And increasing to 100,000 slots, 3,000 periods and 10,000 cars takes  actually speeds up the processing rate:

`python simulation.py  1.66s user 0.31s system 119% cpu 1.645 total` (60,000 o]iterations/.second)

But interestingly increasing to 1,000,000 - 30,000 - 100,000 now takes 148 seconds, 6,700 per second, a lot slower - almost certainly as the arrays no longer fit in physical memory and is now paging. 

The program uses Numpy to maximise speed

It took me about 8 hours (but I enjoyed the challenge).

The task requirement says ". We expect the bulk of the time per each operation to be taken up finding an available slot.".

This is the case with larger simulations. At 10,000 slots, internal setup processes dominate, but at 100,000 slots, then `get_)slots` and Numpy functions called from get_slots take about 82% of the time! (See Appendix A)

I have included a Dockerfile in case there are any environment issues in running this locally.

Python 3.10, MacOS 12.3.1 on an M1 MacBook



## Requirements

**Class Car**

The `Car` class has attributes:

`car_length`, 'units', specified on instance creation

`car_time`, 'units', how long it will be parked for, specified on instance creation

`exit_time` set when it is parked,

`slot`, the car-park slot (first one used), set when it is parked.

**class ParkingPolicy**

Enum, `FirstFit` or `BestFit`

##  The Classes



**Class CarPark**

An operational class that manages the car-park and the methods to find space in it

**class Park**

An operational class that manages the process of parking cars and iterating the 'time' interval, releasing cars whose car parking duration has expired.

**class Simulate** 

In file `simulation.py`, the class that runs the car parking simulation.



## Design Notes

### Class CarPark

The car park must be 10,000 for the simulation.

To simulate this, firstly, I would not use a native Python list. A python list is a linked list of objects, and adding changing and removing elements is a relatively costly process. 

To provide a fast implementation in Python, I have elected to use a Numpy array. The `car_park` is very simple array of 10,000 integers where 0 is `free` and 1 is `busy` (not bool, as the algorithm is based on diff between values). This is in class `CarPark` in `park_implementation.py`. The main methods are:

**`def get_slots`**

This uses np.diff to compare each element in car_park with its neighbour, returning an array of 0 -> in a contiguous segment , -1 -> Boundary from used to free segment, 1 -> boundary from a free to used segment.

By finding the indices of the `1` and `-1` values, and further manipulation, we get the indices and the lengths of each free slot. The method returns these two `ndarray` arrays.

Example, of a car-park of 6 slots:

When empty, the car_park is `0,0,0,0,0,0` and the free-slot-indices list is `[0]`and length-list is `[6]`

When the pattern is `1,0,0,1,1,0`, the index and lengths are `[1,5]` and `[2,1]`identifying 2 free slots of lengths 2 and 1 respectively.

By using a Numpy array we get a huge performance benefit due to:

* Numpy is implemented in C, and the underlying array is a true C array of contiguous memory elements.
* The contiguous memory use means that much of the array will be in the processor cache at all times.
* We dot not change the array length, no expensive append/remove operations, so the memory patter remains unchanged.
* The only operation on the full car_park array is a single-pass diff.
* The complete list of free-slots and length are obtained using Numpy array operations, no Python list manipulation is needed.

**O(x)** will be O(n) as the driving influence is the full car-park scan. The free slot operations are also O(n) with single-pass operations. The size of the arrays used will depend on the car park fragmentation. 

**def get_slot**

Calls `get_slots` on each call, and This is an area that could be improved. Once the free slots are known, it uses Numpy operations to find a suitable free slot (or return None if none available), using either `BestFit` or `FirstFit` algorithms.

Again O(n) but `O` is dependant on the car_park fragmentation, so worst case is 50,000 slots. But still a single pass of a Numpy array to find suitable slots.

**def park**

Uses `get_slot` to park a car, and if OK, then marks the car_park space occupied, by a fast Numpy array index operation `car_park_index[slot : slot + size] = 1`

**O(1)** , no array searches in this method

**def unpark**

Removes a car, very simple `car_park_index[car.slot : car.slot + car.car_length] = 0`, **O(1)**

**def get_utilisation**

Is also **O(n)** from a full scan of the car_park array



# class Park

The main feature of this class is that it records an instance of each car that is parked. If this was a list, we would need to scan the list on each `elapse_period`, so to avoid this, the car instances are in a defaultDict(default=list), where the key is the car's exit_time period, and the value is a list of one or more car instances. 

**def park_car**

When a Car is parked, it is added to the `parked_cars` dict by appending to the list in the entry for this car's exit time. (Using a defaultDict means we do not need to create the list for the first car in each period). This operation is **O(1)**, both list-append and dict-update are O(1).

**def elapse_period** 

This method is now simple and fast - just release all the cars in `parked_cars[self.now]` i.e. all cars whose expire-time has now arrived. It is a full lust scan, so **O(n)** where **n** is the number of cars in the time slot, a function of the number of cars / car-park-size.

**def unpark_car**

Has two forms. 

If `dont_remove_from_list == True`, don;'t bother removing the Car instance from `parked_cars` - this is used only in `elapse_period`, which deletes the entire period car-list. These car instances now have no owner and will be garbage collected.

If `dont_remove_from_list == False`, the default, find and remove the Car instance from `parked__cars`, **O(n)** where **n** is based on the number of cars per period. Only used in testing.

The common elements call `CarPark.unpark`, a fast Numpy array value operation.

In normal simulation, **O(1)**

**def report_utilisation**

Has to scan each element in `cars_parked` dict then each lust in the elements, **O(n)** .

### Simulation.py



**class Simulate** runs the simulation. It iterates over function **get_cars()** which yields a new list of cars on each call with random length and duration. It does not return exactly 300 cars, as 100,000 / 300 = 33.333. To return an integer number of cars, I randomly choose 3 or 4 cars on each call. A more complex algorithm could be provide to make the sum exactly 3000 at the end of the iteration.

The main `simulate` call is in method `__call__` and simply parts each car, reports the iutilkisation then calls `elapse_period` A second loop just runs `elapse_period` until the car_park is empty.



I have not code the generation of a graph, but I have written the iteration data to a CSF file `car_park.csv`and using any spreadsheet program (MacOS Numbers in this case), a plot can be produced as:



![utilisation_plot](/Users/verisk/temp/cars/resources/utilisation_plot.png)



## Testing

File `tests/test_car_park.py` implements Pytest tests, with 99% test coverage. Details of the test coverage are in an HTML report, use a browser to open `htmlcov/index.html`. This is a clickable report that enables line by line analysis of coverage, top level is:

![coverage](/Users/verisk/temp/cars/resources/coverage.png)



# Appendices

## Appendix A - Profile report![profile](/Users/verisk/temp/cars/resources/profile.png)